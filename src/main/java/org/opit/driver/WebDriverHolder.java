package org.opit.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class WebDriverHolder {
    private static WebDriverHolder instance = null;
    private WebDriver driver;

    private WebDriverWait wait;

    private WebDriverHolder() {
        driver = WebDriverFactory.initDriver();
        wait = new WebDriverWait(driver, Duration.ofSeconds(30));
    }

    public synchronized static WebDriverHolder getInstance() {
        if (instance == null){
            instance = new WebDriverHolder();
        }
        return instance;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void quitDriver(){
        if (driver != null){
            driver.quit();
        }
    }

    public WebDriverWait getWait() {
        return wait;
    }
}

