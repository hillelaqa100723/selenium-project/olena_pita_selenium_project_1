package org.opit.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.opit.driver.Browsers;

public class WebDriverFactory {

    public static WebDriver initDriver(Browsers browser) {
        switch (browser) {
            case CHROME -> {
                return new ChromeDriver();
            }
            case FIREFOX -> {
                return new FirefoxDriver();
            }
            case EDGE -> {
                return new EdgeDriver();
            }
        }
        return null;
    }

    public static WebDriver initDriver() {

        String browserToRun = System.getProperty("browserToRun", "chrome");

        {
            try {
                Browsers browser = Browsers.valueOf(browserToRun.toUpperCase());
                return initDriver(browser);
            } catch (IllegalArgumentException e) {
                System.err.println(browserToRun + "is not supported");
                System.exit(-1);
            }
            return null;
        }
    }
}

