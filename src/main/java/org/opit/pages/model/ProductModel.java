package org.opit.pages.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import static org.opit.pages.base.content.BaseContentPage.productDetails;


@Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Accessors(fluent = true)
    public class ProductModel implements Comparable<ProductModel> {
        private String productName;
        private Double price;

        @Override
        public int compareTo(ProductModel o) {
            int compare = productName.compareTo(o.productName());
            return compare == 0 ? price.compareTo(o.price()) : compare;
        }

        public String getSortedProductName() {
            return productName;
        }

        public String getProductName() {
            return productName;
        }

    public Double getProductPrice(){
        return price;
    }
    public static int getProductCount() {
        return productDetails.size();
    }
}

