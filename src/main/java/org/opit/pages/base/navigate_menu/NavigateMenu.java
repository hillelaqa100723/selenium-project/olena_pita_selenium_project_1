package org.opit.pages.base.navigate_menu;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.opit.driver.WebDriverHolder;

public class NavigateMenu {
    public void selectNavigateMenuItem(String topMenuItemName){
        WebDriver driver = WebDriverHolder.getInstance().getDriver();

        WebElement element = driver.findElement(By.xpath("//ul[@class='top-menu notmobile']/li/a[contains(.,'%s')]".formatted(topMenuItemName)));
        element.click();
    }

    public void selectNavigateMenuItemWithSubItem(String topMenuItemName, String subMenuItemName){
        WebDriver driver = WebDriverHolder.getInstance().getDriver();
        WebElement topMenuItemElement = driver.findElement(By.xpath("//ul[@class='top-menu notmobile']/li/a[contains(.,'%s')]".formatted(topMenuItemName)));

        new Actions(driver)
                .moveToElement(topMenuItemElement)
                .perform();
        topMenuItemElement
                .findElement(By.xpath("./../ul//a[contains(.,'%s')]".formatted(subMenuItemName)))
                .click();
    }

}
