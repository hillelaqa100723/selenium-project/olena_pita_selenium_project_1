package org.opit.pages.base.content.computers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.opit.pages.base.BasePage;

public class ProducNotebooktInfoPage extends BasePage {

    @FindBy(xpath = "//div/button[contains(text(),'Add to cart') and @id]")
    public WebElement addToCardButton;

    @FindBy(xpath = "//div[contains(@class,'bar-notification success')]")
    private WebElement infoMessage;


    @FindBy(xpath = "//strong[contains(text(),'Related products')]")
    private WebElement pageTitle;


    public boolean isPageWithItemLoaded() {
        return pageTitle.isDisplayed();
    }

    public void clickOnAddToCartButtonFromProductPage() {
        addToCardButton.click();
    }

    public boolean isInfoMessageDisplayed() {
        return infoMessage.isDisplayed();
    }

}








