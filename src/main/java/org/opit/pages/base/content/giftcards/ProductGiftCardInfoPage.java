package org.opit.pages.base.content.giftcards;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.opit.pages.base.BasePage;

public class ProductGiftCardInfoPage extends BasePage {

    @FindBy(xpath = "//div/button[contains(text(),'Add to cart') and @id]")
    public WebElement addToCardButton;

    @FindBy(xpath = "//div[@id='bar-notification']//p")
    public WebElement errorMessageText;

    @FindBy(xpath = "//div[@id='bar-notification']")
    public static WebElement errorMessage;

    @FindBy(xpath = "//input[@class='recipient-name']")
    public static WebElement RecipientNameField;

    @FindBy(xpath = "//input[@class='recipient-email']")
    public static WebElement RecipientEmailField;

    @FindBy(xpath = "//input[@class='sender-name']")
    public static WebElement SenderNameField;

    @FindBy(xpath = "//input[@class='sender-email']")
    public static WebElement SenderEmailField;


    public static boolean isErrorMessageOnGiftCardsPageVisible() {
       return errorMessage.isDisplayed();
    }

    public void clickOnAddToCartButtonFromGiftCardsPage() {
        addToCardButton.click();
    }

    public String getErrorMessageText() {
        return errorMessageText.getText().trim();
    }
    public static void enterDataToRecipientNameField(String recipientName) {
        RecipientNameField.clear();
        RecipientNameField.sendKeys(recipientName);
    }
    public static void enterDataToRecipientEmailField(String RecipientEmail) {
        RecipientEmailField.clear();
        RecipientEmailField.sendKeys(RecipientEmail);
    }
    public static void enterDataToSenderNameField(String SenderName) {
        SenderNameField.clear();
        SenderNameField.sendKeys(SenderName);
    }
    public static void enterDataToSenderEmailField(String SenderEmail) {
        SenderEmailField.clear();
        SenderEmailField.sendKeys(SenderEmail);
    }
    }

