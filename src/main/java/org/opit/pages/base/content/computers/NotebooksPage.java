package org.opit.pages.base.content.computers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.opit.driver.WebDriverHolder;
import org.opit.pages.base.content.BaseContentPage;
import org.opit.pages.model.ProductModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static org.opit.pages.model.ProductModel.getProductCount;


public class NotebooksPage extends BaseContentPage {

    @FindBy(xpath = "//a[contains(text(),'Next')]")
    private static WebElement nextPageButton;

    @FindBy(xpath = "//a[contains(text(),'Previous')]")
    private static WebElement previousPageButton;

    @FindBy(xpath = "//li[@class='current-page']")
    private static WebElement highlightedPageNumber;

    public static List<ProductModel> getNotebooks() {
        return getProductAsList();
    }

    public static boolean areProductPricesInCurrency(String currencyCode) {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();

        List<WebElement> productPrices = driver.findElements(By.xpath("//span[@class='price actual-price']"));

        for (WebElement productPrice : productPrices) {
            if (!productPrice.getText().contains(currencyCode)) {
                return false;
            }
        }
        return true;
    }

    public static List<ProductModel> getRandomNotebooks(int count) {
        List<ProductModel> allNotebooks = getNotebooks();

        List<ProductModel> selectedNotebooks = new Random()
                .ints(0, allNotebooks.size())
                .distinct()
                .limit(count)
                .mapToObj(allNotebooks::get)
                .toList();

        return selectedNotebooks;
    }

    public void addToCartRandomNotebooks(int count) {
        List<ProductModel> randomNotebooks = getRandomNotebooks(count);
        performActionOnNotebooks(randomNotebooks, "Add to cart");
    }

    public List<String> addToWishlistRandomNotebooks(int count) {
        List<ProductModel> randomNotebooks = getRandomNotebooks(count);
        return performActionAndGetNames(randomNotebooks, "Add to wishlist");
    }

    public List<String> addToComparationListtRandomNotebooks(int count) {
        List<ProductModel> randomNotebooks = getRandomNotebooks(count);
        return performActionAndGetNames(randomNotebooks, "Add to compare list");
    }

    private void performActionOnNotebooks(List<ProductModel> notebooks, String action) {
        for (ProductModel notebook : notebooks) {
            try {
                WebElement actionButton = findActionButton(notebook.getProductName(), action);
                actionButton.click();
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private List<String> performActionAndGetNames(List<ProductModel> notebooks, String action) {

        for (ProductModel notebook : notebooks) {
            try {
                WebElement actionButton = findActionButton(notebook.getProductName(), action);
                actionButton.click();
                addedNotebookNamesForAllListOfNotebooks.add(notebook.getProductName());
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return addedNotebookNamesForAllListOfNotebooks;
    }

    static List<String> addedNotebookNamesForAllListOfNotebooks = new ArrayList<>();

    public static List<String> getAddedNotebookNamesForAllListOfNotebooks() {
        return addedNotebookNamesForAllListOfNotebooks;
    }

    static List<String> addedNotebookPricesForAllListOfNotebooks = new ArrayList<>();
    public static List<String> getAddedNotebookPricesForAllListOfNotebooks() {
        return addedNotebookPricesForAllListOfNotebooks;
    }

    private WebElement findActionButton(String productName, String action) {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();
        String xpath = String.format("//h2[a[text()='%s']]/ancestor::div[@class='product-item']//button[contains(text(),'%s')]", productName, action);
        return driver.findElement(By.xpath(xpath));
    }

    public static List<String> addedNotebookNamesforListOfNotebooksWithoutApple = new ArrayList<>();
    private static List<String> addedNotebookPricesForLostOfNotebookWithoutApple = new ArrayList<>();

    public void chooseAndAddToCartLaptopsExceptApple(int numberOfNotebooksToSelect) {
        if (numberOfNotebooksToSelect <= 0) {
            throw new IllegalArgumentException("Number of notebooks to select should be greater than 0.");
        }

        List<ProductModel> notebooks = getNotebooks();
        List<ProductModel> notAppleNotebooks = notebooks.stream()
                .filter(notebook -> !notebook.getProductName().contains("Apple"))
                .toList();

        if (notAppleNotebooks.size() >= numberOfNotebooksToSelect) {
            List<ProductModel> selectedNotebooks = getRandomElements1(notAppleNotebooks, numberOfNotebooksToSelect);

            for (ProductModel selectedNotebook : selectedNotebooks) {
                clickOnNotebook(selectedNotebook);
                clickOnAddToCartButton(selectedNotebook);
                addedNotebookNamesforListOfNotebooksWithoutApple.add(selectedNotebook.getProductName());
                addedNotebookPricesForLostOfNotebookWithoutApple.add(String.valueOf(selectedNotebook.getProductPrice()));
            }
        } else {
            throw new RuntimeException("Not enough eligible notebooks found except Apple.");
        }
    }

    private static List<ProductModel> getRandomElements1(List<ProductModel> list, int numberOfElements) {
        Random rand = new Random();
        List<Integer> selectedIndices = rand.ints(0, list.size())
                .distinct()
                .limit(numberOfElements)
                .boxed()
                .collect(Collectors.toList());

        return selectedIndices.stream()
                .map(list::get)
                .collect(Collectors.toList());
    }


    private static void clickOnNotebook(ProductModel notebook) {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();
        By notebookLocator = By.xpath("//h2/a[contains(text(), '" + notebook.getProductName() + "')]");

        WebElement notebookElement = driver.findElement(notebookLocator);
        notebookElement.click();
    }

    private static void clickOnAddToCartButton(ProductModel selectedNotebook) {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();
        By addToCartButtonLocator = By.xpath("//div/button[contains(text(),'Add to cart') and @id]");
        WebElement addToCartButton = driver.findElement(addToCartButtonLocator);
        addToCartButton.click();
    }


    public static List<String> getAddedNotebookNamesForListOfNotebooksWithoutApple() {
        return addedNotebookNamesforListOfNotebooksWithoutApple;
    }

    public static List<String> getAddedNotebookPricesForLostOfNotebookWithoutApple() {
        return addedNotebookPricesForLostOfNotebookWithoutApple;
    }

    public static double calculateTotalAmount() {
        List<String> selectedNotebookPrices = getAddedNotebookPricesForLostOfNotebookWithoutApple();
        double totalAmount = 0.0;

        for (String price : selectedNotebookPrices) {
            totalAmount += Double.parseDouble(price);
        }

        return totalAmount;
    }

    public boolean isProductCountCorrect(int expectedCount) {
        return getProductCount() == expectedCount;
    }

    public void clickOnNextPageButton() {
        nextPageButton.click();
    }

    public void clickOnPreviousPageButton() {
        previousPageButton.click();
    }

    public int getHighlightedPageNumber() {
        String pageNumberText = highlightedPageNumber.getText();
        return Integer.parseInt(pageNumberText);
    }

}





