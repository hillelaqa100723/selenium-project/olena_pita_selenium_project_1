package org.opit.pages.base.content.giftcards;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.opit.pages.base.content.BaseContentPage;

import java.util.List;
import java.util.Random;


public class GiftCadrsPage extends BaseContentPage {

    @FindBy(xpath = "//h2[@class='product-title']/a")
    private List<WebElement> physicalCards;

    public void clickOnAnyPhysicalCard() {
        if (!physicalCards.isEmpty()) {
            WebElement firstCard = physicalCards.get(new Random().nextInt(physicalCards.size()));
            firstCard.click();
        } else {
            System.out.println("No physical cards found on the page.");
        }
    }
}





