package org.opit.pages.base.content;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.opit.pages.base.BasePage;
import org.opit.pages.model.ProductModel;
import org.opit.pages.model.enums.SortByDirection;

import java.util.LinkedList;
import java.util.List;


public class BaseContentPage extends BasePage {
    @FindBy(css = ".page-title h1")
    private WebElement contentPageTitle;

    @FindBy(id = "products-orderby")
    private WebElement sortByElement;

    @FindBy(id = "products-pagesize")
    private WebElement displaySizeElement;

    @FindBy(css = ".item-box .details")
    public static List<WebElement> productDetails;


    public BaseContentPage selectSortBy(SortByDirection sortByDirection) {
        new Select(sortByElement)
                .selectByValue(sortByDirection.getValue());
        sleep(1500);
        return new BaseContentPage();
    }


    public BaseContentPage selectDisplayPerPage(int count) {
        new Select(displaySizeElement)
                .selectByValue(String.valueOf(count));
        return new BaseContentPage();
    }

    protected static List<ProductModel> getProductAsList() {
        List<ProductModel> list = new LinkedList<>();

        for (WebElement product : productDetails) {
            String name = product.findElement(By.cssSelector("h2>a")).getText();
            String priceAsString = product.findElement(By.cssSelector(".actual-price")).getText();
            double price = Double.parseDouble(
                    priceAsString
                            .substring(1)
                            .replaceAll(",", ""));
            ProductModel productModel = new ProductModel(name, price);
            list.add(productModel);
        }
        return list;

    }
}
