package org.opit.pages.base.footer;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.opit.pages.base.BasePage;

public class Footer extends BasePage {

   @FindBy(xpath = "//a[contains(text(),'Compare products list')]")
    public WebElement CompareProductsListLink;

    @FindBy(xpath = "//button[@name='addtocartbutton']")
    public WebElement addToCardButton;


    public void selectCompareProductsListLink() {
        CompareProductsListLink.click();
    }
}

