package org.opit.pages.base.wishlist;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.opit.driver.WebDriverHolder;
import org.opit.pages.base.BasePage;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class WishlistPage extends BasePage {

    @FindBy(xpath = "//button[@name='addtocartbutton']")
    public WebElement addToCardButton;

    public static List<WebElement> getWishlistItems() {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();
        return driver.findElements(By.xpath("//tbody/tr"));
    }

    public void selectRandomProduct() {
        List<WebElement> wishlistItems = getWishlistItems();
        if (!wishlistItems.isEmpty()) {
            WebElement randomProduct = wishlistItems.get(new Random().nextInt(wishlistItems.size()));

            WebElement checkbox = randomProduct.findElement(By.xpath(".//input[@name='addtocart']"));

            if (!checkbox.isSelected()) {
                checkbox.click();
            }

            System.out.println("Selected Product: " + randomProduct.findElement(By.xpath(".//input[@name='addtocart']")).getText());

        } else {
            System.out.println("No items found in the wishlist.");
        }
    }

    public static List<String> getWishlistProductNames() {
        List<WebElement> wishlistItems = getWishlistItems();
        return wishlistItems.stream()
                .map(item -> item.findElement(By.xpath("./td[@class='product']/a")).getText())
                .collect(Collectors.toList());
    }

    public void clickOnAddToCartButtonFromWishlistPage() {
        addToCardButton.click();
    }
}
