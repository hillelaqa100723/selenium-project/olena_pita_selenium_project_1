package org.opit.pages.base.main_menu;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.opit.driver.WebDriverHolder;
import org.opit.pages.base.BasePage;
import org.opit.pages.base.shopping_cart.ShoppingCartPage;
import org.opit.pages.base.wishlist.WishlistPage;
import org.opit.pages.login.LoginPage;



public class MainMenu {
    private void selectMenuItem(MenuItems menuItem) {
        findMenuItem(menuItem).click();
    }

    public LoginPage selectLogin() {
        selectMenuItem(MenuItems.LOG_IN);
        return new LoginPage();
    }

    public BasePage selectLogout() {
        selectMenuItem(MenuItems.LOG_OUT);
        return new BasePage();
    }
    public ShoppingCartPage selectShoppingCart() {
        selectMenuItem(MenuItems.SHOPPING_CART);
        return new ShoppingCartPage();
    }

    public WishlistPage selectWishlist() {
        selectMenuItem(MenuItems.WISHLIST);
        return new WishlistPage();
    }

    public Boolean isLogOutMenuItemVisible() {
        return findMenuItem(MenuItems.LOG_OUT)
                .isDisplayed();
    }

    public Boolean isMyAccountMenuItemVisible() {
        return findMenuItem(MenuItems.MY_ACCOUNT)
                .isDisplayed();
    }

    public Boolean isRegisterMenuItemVisible(){
        return findMenuItem(MenuItems.REGISTER).isDisplayed();
    }

    public Boolean isLoginMenuItemVisible(){
        return findMenuItem(MenuItems.LOG_IN).isDisplayed();
    }

    private WebElement findMenuItem(MenuItems menuItem) {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();
        return driver
                .findElement(By.cssSelector(".ico-%s".formatted(menuItem.getValue())));

    }
}
