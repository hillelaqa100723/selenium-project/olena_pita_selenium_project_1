package org.opit.pages.base.main_menu;

public enum MenuItems {
    LOG_IN("login"),
    LOG_OUT("logout"),
    WISHLIST("wishlist"),
    MY_ACCOUNT("account"),
    SHOPPING_CART("cart"),
    REGISTER("register");

    private String value;

    MenuItems(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
