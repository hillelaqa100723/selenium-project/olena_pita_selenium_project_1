package org.opit.pages.base.shopping_cart;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.opit.driver.WebDriverHolder;
import org.opit.pages.base.BasePage;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class ShoppingCartPage extends BasePage {


    public static void changeQuantityOfAnyProduct(int productIndex, int newQuantity) throws InterruptedException {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();

        List<WebElement> productRows = driver.findElements(By.xpath("//table[@class='cart']//tr/td[@class='quantity']"));

        if (!productRows.isEmpty() && productIndex >= 0 && productIndex < productRows.size()) {
            WebElement productRow = productRows.get(productIndex);
            WebElement quantityInput = productRow.findElement(By.xpath("//table[@class='cart']//tr/td[@class='quantity']//input[@class='qty-input']"));

            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
            wait.until(ExpectedConditions.elementToBeClickable(quantityInput));

            quantityInput.click();

            wait.until(ExpectedConditions.elementToBeClickable(quantityInput));

            quantityInput.sendKeys(String.valueOf(newQuantity));
            quantityInput.click();
            quantityInput.sendKeys(Keys.ENTER);

            wait.until(ExpectedConditions.stalenessOf(productRow));

      } else {
           System.out.println("Invalid product index or no products found in the cart.");
        }
    }

    public static double getTotalAmountFromShoppingCart() {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();

        WebElement totalAmountElement = driver.findElement(By.xpath("//td/span[@class='value-summary']/strong"));
        String totalAmountText = totalAmountElement.getText();

        return Double.parseDouble(totalAmountText.replaceAll("[^\\d.]+", ""));
    }

    public static double calculateNewTotalAmount() {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();
        double totalAmount = 0.0;
        List<WebElement> productRows = driver.findElements(By.xpath("//table[@class='cart']//tbody/tr"));

        for (WebElement productRow : productRows) {
            WebElement priceElement = productRow.findElement(By.xpath(".//td[@class='unit-price']//span[@class='product-unit-price']"));
            WebElement quantityElement = productRow.findElement(By.xpath(".//td[@class='quantity']//input[@class='qty-input']"));

            double price = parsePrice(priceElement.getText());
            int quantity = Integer.parseInt(quantityElement.getAttribute("value"));

            totalAmount += price * quantity;
        }

        return totalAmount;
    }

    public static double parsePrice(String priceText) {
        return Double.parseDouble(priceText.replaceAll("[^\\d.]+", ""));
    }

    public void deleteItemFromCart() {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();

        By deleteButtonLocator = By.xpath("//button[@class='remove-btn']");
        java.util.List<WebElement> deleteButtons = driver.findElements(deleteButtonLocator);
        if (!deleteButtons.isEmpty()) {
            deleteButtons.get(0).click();
        } else {
            System.out.println("No items to delete in the cart.");
        }

    }

    public int getNumberOfProducts() {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();
        By productNumberLocator = By.xpath("//td[@class='product']");
        java.util.List<WebElement> productElements = driver.findElements(productNumberLocator);
        return productElements.size();
    }


    public static List<String> getNamesOfNotebooksInShoppingCart() {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();
        List<String> notebookNames = new ArrayList<>();
        List<WebElement> notebookElements = driver.findElements(By.xpath("//a[@class='product-name']"));

        for (WebElement notebookElement : notebookElements) {
            notebookNames.add(notebookElement.getText());
        }
        return notebookNames;
    }
}

