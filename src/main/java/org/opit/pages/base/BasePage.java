package org.opit.pages.base;

import lombok.SneakyThrows;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.opit.driver.WebDriverHolder;
import org.opit.pages.base.main_menu.MainMenu;
import org.opit.pages.base.navigate_menu.NavigateMenu;


public class BasePage {

    @FindBy(id = "customerCurrency")
    private WebElement selectCurrency;

    @FindBy(xpath = "//span[@class='cart-qty']")
    private static WebElement cartQuantity;

    public BasePage() {
        PageFactory.initElements(WebDriverHolder.getInstance().getDriver(), this);
    }


    public MainMenu getMainMenu() {
        return new MainMenu();
    }

    public NavigateMenu getNavigationMenu() {
        return new NavigateMenu();
    }


    public BasePage selectCurrency(Currencies currency) {
        new Select(selectCurrency).selectByVisibleText(currency.getValue());
        return new BasePage();
    }

    public int getShoppingCartValue() {
        String cartValueText = cartQuantity.getText();
        String numericPart = cartValueText.replaceAll("\\D", "");

        try {
            return Integer.parseInt(numericPart);
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    @SneakyThrows
    protected void sleep(long ms) {
        Thread.sleep(ms);
    }
}



