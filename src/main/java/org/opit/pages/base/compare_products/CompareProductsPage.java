package org.opit.pages.base.compare_products;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.opit.pages.base.BasePage;

import java.util.List;
import java.util.stream.Collectors;

public class CompareProductsPage extends BasePage {

    @FindBy(xpath = "//a[@class='clear-list']")
    public WebElement clearListButton;
    @FindBy(xpath = "//div[@class='no-data']")
    public WebElement messageTextForEmptyPage;
    @FindBy(xpath = "//tbody/tr[@class='product-price']/td[@style]")
    private static List<WebElement> compareListItems;

    public List<String> getCompareProductPrices() {
        return compareListItems.stream()
                .map(this::extractNumericValueFromPrice)
                .collect(Collectors.toList());
    }

    private String extractNumericValueFromPrice(WebElement item) {
        String priceText = item.getText().trim();
        double numericValue = Double.parseDouble(priceText.replaceAll("[^0-9.]", ""));
        return String.format("%.1f", numericValue);
    }

    public void clickOnClearListButton() {
        clearListButton.click();
    }

    public String getMessageTextForEmptyPage() {
        return messageTextForEmptyPage.getText().trim();
    }
}

