package org.opit.tests;


import org.opit.pages.base.BasePage;
import org.opit.pages.login.LoginPage;
import org.opit.utils.PropertyReader;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class LoginLogoutTests extends BaseTest {
    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void loginTest() throws InterruptedException {
        String userEmail = PropertyReader.getInstance().getProperty("defaultUser");
        String userPass = PropertyReader.getInstance().getProperty("defaultPass");

        BasePage basePage = new BasePage()
                .getMainMenu()
                .selectLogin();

        assertThat(new LoginPage().isLoginFormLoaded())
                .as("Login Form is loaded.")
                .isTrue();

        LoginPage.login(userEmail, userPass);

        assertThat(basePage.getMainMenu().isLogOutMenuItemVisible())
                .as("Logout menu item should be visible")
                .isTrue();
        assertThat(basePage.getMainMenu().isMyAccountMenuItemVisible())
                .as("MyAccount menu item should be visible")
                .isTrue();

//        assertThat(basePage.getMainMenu().isRegisterMenuItemVisible())
//                .as("Logout menu item should be visible")
//                .isFalse();
//        assertThat(basePage.getMainMenu().isLoginMenuItemVisible())
//                .as("Logout menu item should be visible")
//                .isFalse();

        basePage
                .getMainMenu()
                .selectLogout();
        assertThat(basePage.getMainMenu().isRegisterMenuItemVisible())
                .as("Register menu item should be visible")
                .isTrue();
        assertThat(basePage.getMainMenu().isLoginMenuItemVisible())
                .as("Login menu item should be visible")
                .isTrue();

//        assertThat(basePage.getMainMenu().isLogOutMenuItemVisible())
//                .as("Logout menu item should be visible")
//                .isFalse();
//        assertThat(basePage.getMainMenu().isMyAccountMenuItemVisible())
//                .as("MyAccount menu item should be visible")
//                .isFalse();
    }

}
