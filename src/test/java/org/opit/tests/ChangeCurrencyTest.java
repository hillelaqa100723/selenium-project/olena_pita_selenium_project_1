package org.opit.tests;

import org.opit.pages.base.BasePage;
import org.opit.pages.base.content.computers.NotebooksPage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.opit.pages.base.Currencies.EURO;
import static org.opit.pages.base.Currencies.US_DOLLAR;

public class ChangeCurrencyTest extends BaseTest{
    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void changeCurrency() throws InterruptedException {
        new BasePage()
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers",
                        "Notebooks");
        new BasePage().selectCurrency(EURO);

        assertThat(NotebooksPage.areProductPricesInCurrency("€"))
                .as("All product prices should be in EURO")
                .isTrue();

        new BasePage().selectCurrency(US_DOLLAR);

        assertThat(NotebooksPage.areProductPricesInCurrency("$"))
                .as("All product prices should be in DOLLAR")
                .isTrue();

        }
    }