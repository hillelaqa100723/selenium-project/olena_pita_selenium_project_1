package org.opit.tests;

import org.opit.pages.base.BasePage;
import org.opit.pages.base.content.computers.NotebooksPage;
import org.opit.pages.model.ProductModel;
import org.opit.pages.model.enums.SortByDirection;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class SortingOnPageWithProductsTest extends BaseTest {
    @BeforeMethod
    public void beforeMethod() {
        openUrl();
        new BasePage()
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers",
                        "Notebooks");
    }

    @Test
    public void sortNoteBooks_Z_ATest() {

        List<ProductModel> notebooksBeforeSort = new NotebooksPage().getNotebooks();

        notebooksBeforeSort.forEach(System.out::println);
        System.out.println();

        new NotebooksPage()
                .selectSortBy(SortByDirection.NAME_Z_A);

        List<ProductModel> notebooksAfterSort = new NotebooksPage().getNotebooks();
        Comparator<ProductModel> comparatorZ_To_A = new Comparator<ProductModel>() {
            @Override
            public int compare(ProductModel o1, ProductModel o2) {
                return o2.productName().compareTo(o1.productName());
            }
        };

        Collections.sort(notebooksBeforeSort, comparatorZ_To_A);

        notebooksBeforeSort.forEach(System.out::println);

        assertThat(notebooksAfterSort)
                .isEqualTo(notebooksBeforeSort);
    }

    @Test
    public void sortNoteBooks_A_ZTest() {

        List<ProductModel> notebooksBeforeSort = new NotebooksPage().getNotebooks();

        notebooksBeforeSort.forEach(System.out::println);
        System.out.println();

        new NotebooksPage()
                .selectSortBy(SortByDirection.NAME_A_Z);

        List<ProductModel> notebooksAfterSort = new NotebooksPage().getNotebooks();
        Comparator<ProductModel> comparatorA_To_Z = new Comparator<ProductModel>() {
            @Override
            public int compare(ProductModel o1, ProductModel o2) {
                return o1.productName().compareTo(o2.productName());
            }
        };

        Collections.sort(notebooksBeforeSort, comparatorA_To_Z);

        notebooksBeforeSort.forEach(System.out::println);

        assertThat(notebooksAfterSort)
                .isEqualTo(notebooksBeforeSort);
    }

    @Test
    public void sortNoteBooksHigh_LowTest() {

        List<ProductModel> notebooksBeforeSort = new NotebooksPage().getNotebooks();

        notebooksBeforeSort.forEach(System.out::println);
        System.out.println();

        new NotebooksPage().selectSortBy(SortByDirection.PRICE_HIGH_TO_LOW);

        List<ProductModel> notebooksAfterSort = new NotebooksPage().getNotebooks();
        Comparator<ProductModel> comparatorH_L = new Comparator<ProductModel>() {
            @Override
            public int compare(ProductModel o1, ProductModel o2) {
                return o2.price().compareTo(o1.price());
            }
        };

        Collections.sort(notebooksBeforeSort, comparatorH_L);

        notebooksBeforeSort.forEach(System.out::println);

        assertThat(notebooksAfterSort)
                .asList()
                .isSorted()
                .isEqualTo(notebooksBeforeSort);

    }

    @Test
    public void sortNoteBooksLow_HighTest() {

        List<ProductModel> notebooksBeforeSort = new NotebooksPage().getNotebooks();

        notebooksBeforeSort.forEach(System.out::println);
        System.out.println();

        new NotebooksPage().selectSortBy(SortByDirection.PRICE_LOW_TO_HIGH);

        List<ProductModel> notebooksAfterSort = new NotebooksPage().getNotebooks();
        Comparator<ProductModel> comparatorL_H = new Comparator<ProductModel>() {
            @Override
            public int compare(ProductModel o1, ProductModel o2) {
                return o1.price().compareTo(o2.price());
            }
        };

        Collections.sort(notebooksBeforeSort, comparatorL_H);

        notebooksBeforeSort.forEach(System.out::println);

        assertThat(notebooksAfterSort)
                .asList()
                .isSorted()
                .containsExactlyElementsOf(notebooksBeforeSort);

    }

}
