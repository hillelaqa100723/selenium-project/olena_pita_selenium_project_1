package org.opit.tests;

import org.opit.pages.base.BasePage;
import org.opit.pages.base.content.computers.NotebooksPage;
import org.opit.pages.base.shopping_cart.ShoppingCartPage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ChangeTheNumberOfItemsInShoppingCardTest extends BaseTest {
    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void changeNumberOfItemsInShoppingCart() throws InterruptedException {
        new BasePage()
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers",
                        "Notebooks");

        int initialCartValue = new BasePage().getShoppingCartValue();

        new NotebooksPage().chooseAndAddToCartLaptopsExceptApple(2);

        Thread.sleep(6000);

        new BasePage().getMainMenu().selectShoppingCart();

        List<String> expectedNames = NotebooksPage.getAddedNotebookNamesForListOfNotebooksWithoutApple();
        List<String> actualNames = ShoppingCartPage.getNamesOfNotebooksInShoppingCart();

        assertThat(actualNames)
                .as("All notebook names added recently should be present in the shopping cart")
                .containsExactlyElementsOf(expectedNames);


        double expectedTotalAmount = Double.parseDouble(String.valueOf(NotebooksPage.calculateTotalAmount()));
        double actualTotalAmount = ShoppingCartPage.getTotalAmountFromShoppingCart();

        assertThat(actualTotalAmount)
                .as("Total amount of the order should be correct")
                .isEqualTo(expectedTotalAmount);


        int updatedCartValue = new BasePage().getShoppingCartValue();
        assertThat(updatedCartValue)
                .as("Number of products on Shopping cart should be correct")
                .isEqualTo(initialCartValue + 2);

        Thread.sleep(10000);

        ShoppingCartPage.changeQuantityOfAnyProduct(1, 2);

        assertThat(new ShoppingCartPage().getShoppingCartValue())
                .as("Shopping cart value should be updated")
                .isEqualTo(22);  // Provide the expected updated cart value


        double expectedNewTotalAmount = Double.parseDouble(String.valueOf(ShoppingCartPage.calculateNewTotalAmount()));
        double actualNewTotalAmount = new ShoppingCartPage().getTotalAmountFromShoppingCart();

        assertThat(actualNewTotalAmount)
                .as("Total amount of the order should be correct")
                .isEqualTo(expectedNewTotalAmount);
    }
}
