package org.opit.tests;

import org.opit.pages.base.BasePage;
import org.opit.pages.base.content.computers.NotebooksPage;
import org.opit.pages.model.ProductModel;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.assertNotEquals;

public class PaginationTest extends BaseTest{

    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void paginationTest() throws InterruptedException {
        new BasePage()
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers",
                        "Notebooks");

        new NotebooksPage().selectDisplayPerPage(3);
        Thread.sleep(2000);

        List<ProductModel> notebooksOnCurrentPage = new NotebooksPage().getNotebooks();

        new NotebooksPage().clickOnNextPageButton();

        Thread.sleep(3000);

        assertThat(new NotebooksPage().getHighlightedPageNumber())
                .as("Current page should be 2")
                .isEqualTo(2);

        List<ProductModel> notebooksOnNextPage = new NotebooksPage().getNotebooks();

        assertNotEquals(notebooksOnNextPage, notebooksOnCurrentPage, "The lists should not be equal when move to Next page");

        new NotebooksPage().clickOnPreviousPageButton();
        Thread.sleep(3000);

        List<ProductModel> notebooksOnPreviousPage = new NotebooksPage().getNotebooks();

        assertThat(new NotebooksPage().getHighlightedPageNumber())
                .as("Current page should be 1")
                .isEqualTo(1);

        assertNotEquals(notebooksOnNextPage, notebooksOnPreviousPage, "The lists should not be equal when move to Previous page");
    }
}
