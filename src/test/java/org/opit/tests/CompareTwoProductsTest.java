package org.opit.tests;

import org.opit.pages.base.BasePage;
import org.opit.pages.base.compare_products.CompareProductsPage;
import org.opit.pages.base.content.computers.NotebooksPage;
import org.opit.pages.base.footer.Footer;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CompareTwoProductsTest extends BaseTest {
    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void compareTwoProducts() throws InterruptedException {
        new BasePage()
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers",
                        "Notebooks");

        NotebooksPage NotebooksPage = new NotebooksPage();
        NotebooksPage.addToComparationListtRandomNotebooks(2);

        new Footer().selectCompareProductsListLink();

        List<String> addedNotebookPrices = new NotebooksPage().getAddedNotebookPricesForAllListOfNotebooks();
        List<String> compareProductPrices = new CompareProductsPage().getCompareProductPrices();

        assertThat(compareProductPrices)
                .as("All notebook prices added to the Compare List should be correct on the Compare page")
                .containsExactlyInAnyOrderElementsOf(addedNotebookPrices);

       new CompareProductsPage().clickOnClearListButton();

        assertThat(new CompareProductsPage().getMessageTextForEmptyPage())
                .as("Error message text should be 'You have no items to compare.'")
                .isEqualTo("You have no items to compare.");
    }
}
