package org.opit.tests;

import org.opit.pages.base.BasePage;
import org.opit.pages.base.content.giftcards.GiftCadrsPage;
import org.opit.pages.base.content.giftcards.ProductGiftCardInfoPage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ErrorMessageInGiftCardTest extends BaseTest{
    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void errorMessageInGiftCard() throws InterruptedException {
        new BasePage()
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItem("Gift Cards");

        Thread.sleep(8000);


        new GiftCadrsPage().clickOnAnyPhysicalCard();


        Thread.sleep(8000);

       new ProductGiftCardInfoPage().clickOnAddToCartButtonFromGiftCardsPage();

        Thread.sleep(8000);

        assertThat(ProductGiftCardInfoPage.isErrorMessageOnGiftCardsPageVisible())
                .as(" Error Message should be visible")
                .isTrue();
        assertThat(new ProductGiftCardInfoPage().getErrorMessageText())
                .as("Error message text should be 'Enter valid recipient name'")
                .isEqualTo("Enter valid recipient name");


        ProductGiftCardInfoPage.enterDataToRecipientNameField("Test Name");
        ProductGiftCardInfoPage.enterDataToSenderNameField("");

        int initialCartValue = new BasePage().getShoppingCartValue();

        new ProductGiftCardInfoPage().clickOnAddToCartButtonFromGiftCardsPage();

        assertThat(ProductGiftCardInfoPage.isErrorMessageOnGiftCardsPageVisible())
                .as(" Error Message should be visible")
                .isTrue();
        assertThat(new ProductGiftCardInfoPage().getErrorMessageText())
                .as("Error message text should be 'Enter valid sender name'")
                .isEqualTo("Enter valid sender name");

        Thread.sleep(10000);

        ProductGiftCardInfoPage.enterDataToSenderNameField("John Smith");

        new ProductGiftCardInfoPage().clickOnAddToCartButtonFromGiftCardsPage();

        Thread.sleep(6000);

        int updatedCartValue = new BasePage().getShoppingCartValue();
        assertThat(updatedCartValue)
                .as("Number of products on Shopping cart should be correct")
                .isEqualTo(initialCartValue + 1);

    }

}
