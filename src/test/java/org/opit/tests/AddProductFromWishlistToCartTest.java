package org.opit.tests;

import org.opit.pages.base.BasePage;
import org.opit.pages.base.content.computers.NotebooksPage;
import org.opit.pages.base.wishlist.WishlistPage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class AddProductFromWishlistToCartTest extends BaseTest {

    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void addProductsFromWishListToCartTest() throws InterruptedException {

        new BasePage()
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser();
        new BasePage()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers", "Notebooks");

        NotebooksPage notebooksPage = new NotebooksPage();
        notebooksPage.addToCartRandomNotebooks(2);

        Thread.sleep(6000);

        notebooksPage = new NotebooksPage();
        notebooksPage.addToWishlistRandomNotebooks(3);

        Thread.sleep(6000);

        new BasePage().getMainMenu().selectWishlist();

        Thread.sleep(6000);


        List<String> addedNotebookNames = new NotebooksPage().getAddedNotebookNamesForAllListOfNotebooks();
        List<String> wishlistProductNames = WishlistPage.getWishlistProductNames();

        assertThat(wishlistProductNames)
                .as("All notebook names added to the wishlist should be present in the wishlist")
                .containsExactlyInAnyOrderElementsOf(addedNotebookNames);

        Thread.sleep(6000);

        WishlistPage wishlistPage = new WishlistPage();
        wishlistPage.selectRandomProduct();
        wishlistPage.clickOnAddToCartButtonFromWishlistPage();
    }

}
