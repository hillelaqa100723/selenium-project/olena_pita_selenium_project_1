package org.opit.tests;

import org.opit.pages.base.BasePage;
import org.opit.pages.base.content.computers.NotebooksPage;
import org.opit.pages.model.ProductModel;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


public class DisplayPerPageOnProductsPageTest extends BaseTest {

    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void changePerPageOnProductPage() throws InterruptedException {
        new BasePage()
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers",
                        "Notebooks");

        List<ProductModel> notebooks = new NotebooksPage().getNotebooks();

        new NotebooksPage().selectDisplayPerPage(3);
        Thread.sleep(2000);

        assertThat(new NotebooksPage().isProductCountCorrect(3))
                .as("Number of products on page should be correct for per page 3")
                .isTrue();

        new NotebooksPage().selectDisplayPerPage(6);
        Thread.sleep(2000);

        assertThat(new NotebooksPage().isProductCountCorrect(6))
                .as("Number of products on page should be correct for per page 6")
                .isTrue();

    }
}
