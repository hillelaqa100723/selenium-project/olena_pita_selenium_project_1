package org.opit.tests;

import org.opit.pages.base.BasePage;
import org.opit.pages.base.content.computers.NotebooksPage;
import org.opit.pages.base.content.computers.ProducNotebooktInfoPage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class AddItemToCardTest extends BaseTest {
    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void getProducts() throws InterruptedException {
        new BasePage()
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers",
                        "Notebooks");

        new NotebooksPage().chooseAndAddToCartLaptopsExceptApple(1);

        Thread.sleep(2000);

        assertThat(new ProducNotebooktInfoPage().isPageWithItemLoaded())
                .as("Page with item is loaded.")
                .isTrue();

        int initialCartValue = new BasePage().getShoppingCartValue();

        new ProducNotebooktInfoPage().clickOnAddToCartButtonFromProductPage();

        Thread.sleep(2000);

        assertThat(new ProducNotebooktInfoPage()
                .isInfoMessageDisplayed())
                .isTrue();

        Thread.sleep(6000);

        int updatedCartValue = new ProducNotebooktInfoPage().getShoppingCartValue();
        assertThat(updatedCartValue)
                .as("Shopping cart value increased by 1.")
                .isEqualTo(initialCartValue + 1);
    }
}






