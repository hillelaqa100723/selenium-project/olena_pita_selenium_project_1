package org.opit.tests;

import org.opit.pages.base.BasePage;
import org.opit.pages.base.content.computers.NotebooksPage;
import org.opit.pages.base.shopping_cart.ShoppingCartPage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class DeleteItemFromCartTest extends BaseTest {

    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test

    public void deleteProduct() throws InterruptedException {
        new BasePage()
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers",
                        "Notebooks");

        int initialCartValue = new BasePage().getShoppingCartValue();

        new NotebooksPage().chooseAndAddToCartLaptopsExceptApple(2);

        Thread.sleep(6000);

        new BasePage().getMainMenu().selectShoppingCart();

        int initialNumberOfProducts = new ShoppingCartPage().getNumberOfProducts();

        int updatedCartValue = new BasePage().getShoppingCartValue();
        assertThat(updatedCartValue)
                .as("Number of products on Shopping cart should be correct")
                .isEqualTo(initialCartValue + 2);

        new ShoppingCartPage().deleteItemFromCart();
        new ShoppingCartPage().getNumberOfProducts();

        int updatedNumberOfProducts = new ShoppingCartPage().getNumberOfProducts();
        assertThat(updatedNumberOfProducts)
                .as("Number of products in Shopping card should be correct")
                .isEqualTo(initialNumberOfProducts - 1);
    }

}

